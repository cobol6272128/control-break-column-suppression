       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONBRE1.
       AUTHOR. JIRATIP.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT 100-INPUT-FILE ASSIGN TO "DATA2.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.

           SELECT 200-OUTPUT-FILE ASSIGN TO "REPORT2.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-OUTPUT-FILE-STATUS.

       DATA DIVISION.
       FILE SECTION.
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  INPUT-FILE-RECORD.
           05 A-ID                          PIC X(2).
           05 B-ID                          PIC X(2).
           05 COUNTER                       PIC 9(3).
       
       FD  200-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  OUTPUT-FILE-RECORD               PIC X(20).

       WORKING-STORAGE SECTION.
       01  WS-INPUT-FILE-STATUS             PIC X(2).
           88 FILE-OK                       VALUE "00".
           88 FILE-AT-END                   VALUE "10".

       01  WS-OUTPUT-FILE-STATUS            PIC X(2).
           88 FILE-OK                       VALUE "00".
           88 FILE-AT-END                   VALUE "10".

       01  WS-CALCULATION.
           05 WS-A-CB                       PIC X(2) VALUE SPACE.
           05 WS-A-SHOW                     PIC X(1) VALUE "S".
           05 WS-A-TOTAL                    PIC 9(5) VALUE ZERO.
           05 WS-B-CB                       PIC X(2) VALUE SPACE.
           05 WS-B-TOTAL                    PIC 9(5) VALUE ZERO.
           05 WS-TOTAL                      PIC 9(5) VALUE ZERO.
           05 WS-READ-COUNT-INPUT           PIC 9(5) VALUE ZERO.

       01  RPT-FORMAT.
           05 RPT-DETAIL-A.
              10 FILLER                     PIC X(6) VALUE "TOTAL ".
              10 RPT-A-ID                   PIC X(2).
              10 FILLER                     PIC X(2) VALUE SPACE.
              10 RPT-A-TOTAL                PIC ZZZZ9.

           05 RPT-DETAIL-B.
              10 RPT-A-ID                   PIC X(2).
              10 FILLER                     PIC X(4) VALUE SPACE.
              10 RPT-B-ID                   PIC X(2).
              10 FILLER                     PIC X(2) VALUE SPACE.
              10 RPT-B-TOTAL                PIC ZZZZ9.

           05 RPT-FOOTER.
              10 FILLER                     PIC X(5) 
                 VALUE "TOTAL".
              10 FILLER                     PIC X(5) VALUE SPACE.
              10 RPT-TOTAL                  PIC ZZZZ9.


       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL  THRU 1000-EXIT
           PERFORM 2000-PROCESS  THRU 2000-EXIT 
                   UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
           PERFORM 3000-END      THRU 3000-EXIT
           GOBACK
           .

       1000-INITIAL.
           PERFORM 1100-OPEN-INPUT THRU 1100-EXIT
           PERFORM 1200-OPEN-OUTPUT THRU 1200-EXIT
           MOVE ZEROS TO WS-TOTAL
      *     DISPLAY RPT-HEADER
           PERFORM 8000-READ THRU 8000-EXIT
           .
       1000-EXIT.
           EXIT.       

       1100-OPEN-INPUT.
           OPEN INPUT 100-INPUT-FILE 
           IF FILE-OK OF WS-INPUT-FILE-STATUS
              CONTINUE
           ELSE  
              DISPLAY "***** CONBRE1 ABEND *****"
                 UPON CONSOLE
              DISPLAY "*PARA 1100-OPEN-INPUT*"
                 UPON CONSOLE
              DISPLAY "*FILE STATUS = " WS-INPUT-FILE-STATUS
                 UPON CONSOLE   
              DISPLAY "***** CONBRE1 ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           .
       1100-EXIT.
           EXIT.

       1200-OPEN-OUTPUT.
           OPEN OUTPUT 200-OUTPUT-FILE 
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS
              CONTINUE
           ELSE  
              DISPLAY "***** CONBRE1 ABEND *****"
                 UPON CONSOLE
              DISPLAY "*PARA 1200-OPEN-OUTPUT*"
                 UPON CONSOLE
              DISPLAY "*FILE STATUS = " WS-OUTPUT-FILE-STATUS
                 UPON CONSOLE   
              DISPLAY "***** CONBRE1 ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           .
       1200-EXIT.
           EXIT.

       2000-PROCESS.
           MOVE A-ID TO WS-A-CB
           MOVE ZEROS TO WS-A-TOTAL
           PERFORM 2100-PROCESS-A THRU 2100-EXIT 
                   UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
                   OR WS-A-CB NOT= A-ID

      *     DISPLAY A-ID " " WS-A-CB " " WS-A-TOTAL
           MOVE "S" TO WS-A-SHOW
           MOVE WS-A-CB         TO RPT-A-ID    OF RPT-DETAIL-A
           MOVE WS-A-TOTAL      TO RPT-A-TOTAL OF RPT-DETAIL-A
      *     DISPLAY RPT-DETAIL
           MOVE RPT-DETAIL-A TO OUTPUT-FILE-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT
           .
       2000-EXIT.
           EXIT.

       2100-PROCESS-A.
           MOVE B-ID TO WS-B-CB
           MOVE ZEROS TO WS-B-TOTAL
           PERFORM 2200-PROCESS-B THRU 2200-EXIT 
                   UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
                   OR WS-A-CB NOT= A-ID
                   OR WS-B-CB NOT= B-ID
      *     DISPLAY A-ID " " WS-A-CB " " WS-A-TOTAL
           IF WS-A-SHOW = "S"
              MOVE WS-A-CB         TO RPT-A-ID OF RPT-DETAIL-B
              MOVE "H" TO WS-A-SHOW
           ELSE  
              MOVE SPACE           TO RPT-A-ID OF RPT-DETAIL-B
           END-IF
           MOVE WS-B-CB            TO RPT-B-ID OF RPT-DETAIL-B
           MOVE WS-B-TOTAL         TO RPT-B-TOTAL OF RPT-DETAIL-B
      *     DISPLAY RPT-DETAIL
           MOVE RPT-DETAIL-B TO OUTPUT-FILE-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT     
           .
       2100-EXIT.
           EXIT.

       2200-PROCESS-B.
      *     DISPLAY A-ID " " COUNTER
           ADD COUNTER TO WS-TOTAL
           ADD COUNTER TO WS-A-TOTAL
           ADD COUNTER TO WS-B-TOTAL
           PERFORM 8000-READ THRU 8000-EXIT
           .
       2200-EXIT.
           EXIT.

       3000-END.
           MOVE WS-TOTAL TO RPT-TOTAL
      *     DISPLAY RPT-FOOTER
           MOVE RPT-FOOTER TO OUTPUT-FILE-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT
           CLOSE 100-INPUT-FILE 200-OUTPUT-FILE

           DISPLAY "READ " WS-READ-COUNT-INPUT " RECORDS"
           .
       3000-EXIT.
           EXIT.

       7000-WRITE.
           WRITE OUTPUT-FILE-RECORD
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS
              CONTINUE
           ELSE  
              DISPLAY "***** CONBRE1 ABEND *****"
                 UPON CONSOLE
              DISPLAY "*PARA 7000-WRITE*"
                 UPON CONSOLE
              DISPLAY "*FILE STATUS = " WS-OUTPUT-FILE-STATUS
                 UPON CONSOLE   
              DISPLAY "***** CONBRE1 ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           .
       7000-EXIT.
           EXIT.


       8000-READ.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-INPUT-FILE-STATUS
              ADD 1 TO WS-READ-COUNT-INPUT
           ELSE
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS
                 CONTINUE
              ELSE
                 DISPLAY "***** CONBRE1 ABEND *****"
                    UPON CONSOLE
                 DISPLAY "*PARA 8000-READ*"
                    UPON CONSOLE
                 DISPLAY "*FILE STATUS = " WS-INPUT-FILE-STATUS
                    UPON CONSOLE   
                 DISPLAY "***** CONBRE1 ABEND *****"
                    UPON CONSOLE
                 STOP RUN
              END-IF
           END-IF
           .
       8000-EXIT.
           EXIT.

